#!/usr/bin/python3
import sys
import os
from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import QPainter, QImage, QBrush, QColor
from PyQt5 import QtWidgets, QtCore, QtGui, QtSvg, QtMultimedia
from PyQt5.QtMultimedia import QSound
from PyQt5.QtCore import QSize, Qt, QTimer
from PyQt5.QtSvg import QSvgWidget, QSvgRenderer


def getwidget(penguin, iconintab, textintab, iconsizexy):
    game = Game()
    return game


def getpath(path):
    root, exe = os.path.split(__file__)
    return root + '/' + path


class Game(QWidget):
    def __init__(self):
        super().__init__()
        self.Racquetssize_x = 0
        self.Racquetssize_y = 0
        self.Racquet_1_y = 0
        self.Racquet_2_y = 0
        self.lastx = 'Minus'
        self.lasty = 'Minus'
        self.x_arena = 0.
        self.y_arena = 0.
        self.x = 4.
        self.y = 1.
        self.t = 2.
        self.gameproc_x = 0.
        self.gameproc_y = 0.
        self.ball_x = 250.
        self.ball_y = 100.
        self.ball_x_rotate = 'Plus'
        self.ball_y_rotate = 'Plus'
        self.ballsize_x = 0.
        self.ballsize_y = 0.
        self.timer = QTimer()
        self.timer.timeout.connect(self.timeout)
        self.timer.setInterval(20)
        self.timer.start()

        self.timer1 = QTimer()
        self.timer1.timeout.connect(self.timeout1)
        self.timer1.setInterval(100)
        self.timer1.start()

    def hit(self):
        print('Hit sound is playing.')
        hit = getpath('hit.wav')
        QSound.play(hit)

    def imageload(self):
        renderer_s1 = QSvgRenderer(getpath('Ball_1.svg'))
        self.ball_image = QImage(int(self.ballsize_x), int(self.ballsize_y), QImage.Format_ARGB32)
        self.ball_image.fill(0x00000000)
        painter = QPainter(self.ball_image)
        renderer_s1.render(painter)

        renderer_s2 = QSvgRenderer(getpath('Table.svg'))
        self.table_image = QImage(int(self.x_arena), int(self.y_arena), QImage.Format_ARGB32)
        self.table_image.fill(0x00000000)
        painter = QPainter(self.table_image)
        renderer_s2.render(painter)

        renderer_s3 = QSvgRenderer(getpath('Racquet_1.svg'))
        self.Racquet_1_image = QImage(int(self.Racquetssize_x), int(self.Racquetssize_y), QImage.Format_ARGB32)
        self.Racquet_1_image.fill(0x00000000)
        painter = QPainter(self.Racquet_1_image)
        renderer_s3.render(painter)

        renderer_s4 = QSvgRenderer(getpath('Racquet_2.svg'))
        self.Racquet_2_image = QImage(int(self.Racquetssize_x), int(self.Racquetssize_y), QImage.Format_ARGB32)
        self.Racquet_2_image.fill(0x00000000)
        painter = QPainter(self.Racquet_2_image)
        renderer_s4.render(painter)

    def timeout1(self):
        self.Rocquets()

    def Rocquets(self):
        if QApplication.instance().keyboardModifiers() & Qt.Key_A and int(self.Racquet_1_y) < 201:
            self.Racquet_1_y = int(self.Racquet_1_y) + 1
            print('Key A is pressed now.')
        if QApplication.instance().keyboardModifiers() & Qt.Key_Z and int(self.Racquet_1_y) > 0:
            self.Racquet_1_y = int(self.Racquet_1_y) - 1
            print('Key Z is pressed now.')
        if QApplication.instance().keyboardModifiers() & Qt.Key_K and int(self.Racquet_2_y) < 201:
            self.Racquet_2_y = int(self.Racquet_2_y) + 1
            print('Key K is pressed now.')
        if QApplication.instance().keyboardModifiers() & Qt.Key_M and int(self.Racquet_2_y) > 0:
            self.Racquet_2_y = int(self.Racquet_2_y) - 1
            print('Key M is pressed now.')

    def timeout(self):
        self.update()

    def game(self, width, height):
        hit_detect = False
        self.x_arena = width
        self.y_arena = height
        self.gameproc_x = 0
        self.gameproc_y = 0
        self.gameproc_x = width / 500
        self.gameproc_y = height / 200
        self.ballsize_x = self.gameproc_x * 25
        self.ballsize_y = self.gameproc_y * 25
        self.Racquetssize_x = self.gameproc_x * 9
        self.Racquetssize_y = self.gameproc_y * 40

        if self.ball_x * self.gameproc_x >= self.x_arena or self.ball_x * self.gameproc_x <= 0 + self.ballsize_x:
            self.x = - self.x
            hit_detect = True

        if self.ball_y * self.gameproc_y >= self.y_arena or self.ball_y * self.gameproc_y <= 0 + self.ballsize_y:
            self.y = - self.y
            hit_detect = True

        self.ball_x += self.x * self.t
        self.ball_y += self.y * self.t

        if hit_detect:
            self.hit()

        print(self.ball_x, self.ball_y)
            
    def drawball(self, painter):
        painter.drawImage(0, 0, self.table_image)
        painter.drawImage(int(self.ball_x * self.gameproc_x - self.ballsize_x), int(self.ball_y * self.gameproc_y - self.ballsize_y), self.ball_image)
        painter.drawImage(int(3 * self.gameproc_x), int(self.Racquet_1_y * self.gameproc_y), self.Racquet_1_image)
        painter.drawImage(int(489 * self.gameproc_x), int(self.Racquet_2_y * self.gameproc_y), self.Racquet_2_image)

    def resizeEvent(self, event):
        print('Do it.')
        self.imageload()

    def paintEvent(self, event):
        painter = QPainter(self)
        self.game(painter.device().width(), painter.device().height())
##        painter.fillRect(0, 0, painter.device().width(), painter.device().height(), QBrush(QColor(50, 50, 50)))
        self.drawball(painter)
        painter.end()
