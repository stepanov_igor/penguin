from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import pyqtSignal, pyqtSlot, QThread, Qt, QObject, QTimer
from PyQt5.QtGui import QPainter, QImage
from PyQt5.QtSvg import QSvgRenderer
import os
from openal import *


def getwidget(penguin, iconintab, textintab, iconsizexy):
    paint = Paint()
    return paint

def getpath(path):
    root, exe = os.path.split(__file__)
    return root + '/' + path
        
class Paint(QWidget):
    def __init__(self):
        super().__init__()
        self.arena_width = self.frameGeometry().width()
        self.arena_height = self.frameGeometry().height()
        self.write_object = WriteObject()
        self.write_thread = QThread(self)
        self.write_thread.start()
        self.write_object.moveToThread(self.write_thread)

    def resizeEvent(self, event):
        self.arena_width = self.frameGeometry().width()
        self.arena_height = self.frameGeometry().height()
        self.arena_percent_x = self.arena_width / 500
        self.arena_percent_y = self.arena_height / 200
        self.load()
        self.paint()

    def paint(self):
        self.update()

    def paintEvent(self, event):
        painter = QPainter(self)
        self.draw(painter)
        painter.end()

    def load(self):

    def draw(self, painter):


class WriteObject(QObject):
    play_signal = pyqtSignal()
    load_signal = pyqtSignal()

    def __init__(self):
        super().__init__()
        self.play_signal.connect(self.play, Qt.QueuedConnection)
        self.load_signal.connect(self.load, Qt.QueuedConnection)
        pyoggSetStreamBufferSize(4096*4)
        oalSetStreamBufferCount(4)

    @pyqtSlot()
    def load(self):
       

    @pyqtSlot()
    def play(self):
       
