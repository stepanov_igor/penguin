from PyQt5.QtWidgets import QWidget, QPushButton, QLabel, QVBoxLayout
from PyQt5.QtCore import pyqtSignal, pyqtSlot, QThread, Qt, QObject, QTimer
from PyQt5.QtGui import QPainter, QImage, QFont
from PyQt5.QtSvg import QSvgRenderer
import os

class Image:
    def __init__(self, x, y, w, h, img, path, visible):
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.img = img
        self.path = path
        self.visible = visible

def getwidget(penguin, iconintab, textintab, iconsizexy):
    menu = MainMenu([Image(100, 100, 100, 100, None, getpath('ball/ball_1.svg'), True)])
    return menu

def getpath(path):
    root, exe = os.path.split(__file__)
    return root + '/' + path


# class Paint:
#     pass


class Paint(QWidget):
    def __init__(self, loadlist):
        super().__init__()
        self.loadlist = loadlist
        self.arena_percent_x = int()
        self.arena_percent_y = int()
        self.arena_width = self.frameGeometry().width()
        self.arena_height = self.frameGeometry().height()

    def resizeEvent(self, event):
        self.arena_width = self.frameGeometry().width()
        self.arena_height = self.frameGeometry().height()
        self.arena_percent_x = self.arena_width / 500
        self.arena_percent_y = self.arena_height / 200
        print(self.arena_percent_x, self.arena_percent_y)
        self.load()
        self.paint()

    def paint(self):
        self.update()

    def paintEvent(self, event):
        print('1')
        painter = QPainter(self)
        self.draw(painter)
        painter.end()

    def load(self):
        for line in self.loadlist:
            print(int(line.w * self.arena_percent_x), int(line.y * self.arena_percent_y))
            renderer = QSvgRenderer(line.path)
            line.img = QImage(int(line.w * self.arena_percent_x), int(line.y * self.arena_percent_y), QImage.Format_ARGB32)
            line.img.fill(0x00000000)
            painter = QPainter(line.img)
            renderer.render(painter)

    def draw(self, painter):
        for line in self.loadlist:
            if line.visible != None:
                painter.drawImage(line.x * self.arena_percent_x, line.y * self.arena_percent_y, line.img)


class MainMenu(Paint):
    def __init__(self, loadlist):
        super(MainMenu, self).__init__(loadlist)
        # self.paint = Paint(self.loadlist)
        self.load()
        self.timer = QTimer()
        self.timer.timeout.connect(self.timeout)
        self.timer.setInterval(20)
        self.timer.start()

    def timeout(self):
        self.paint()

