from openal import *
import sys
import os
import time
from PyQt5.QtWidgets import QWidget, QPushButton
from PyQt5.QtCore import pyqtSignal, pyqtSlot, QThread, Qt, QObject, QTimer
from PyQt5.QtGui import QPainter, QImage
from PyQt5.QtSvg import QSvgRenderer


def getwidget(penguin, iconintab, textintab, iconsizexy):
    game = Game()
    return game

def getpath(path):
    root, exe = os.path.split(__file__)
    return root + '/' + path


class WriteObject(QObject):
    play_signal = pyqtSignal(str)

    def __init__(self):
        super().__init__()
        self.play_signal.connect(self.play, Qt.QueuedConnection)
        pyoggSetStreamBufferSize(4096*4)
        oalSetStreamBufferCount(4)

    @pyqtSlot(str)
    def play(self, path):
        self.sourceStream = oalStream(path)
        self.sourceStream.play()
        while self.sourceStream.get_state() == AL_PLAYING:
            self.sourceStream.update()
        return True


class Game(QWidget):
    def __init__(self):
        super().__init__()
        self.CustomEvent = None
        self.write_thread = None
        self.init_ui()

        self.keylist = set()
        self.setFocusPolicy(Qt.StrongFocus)
        self.Racquetssize_x = 0
        self.Racquetssize_y = 0
        self.Racquet_1_y = 80
        self.Racquet_2_y = 80
        self.lastx = 0
        self.lasty = 0
        self.x_arena = 0.
        self.y_arena = 0.
        self.x = 4.
        self.y = 1.
        self.t = 2.
        self.gameproc_x = 0.
        self.gameproc_y = 0.
        self.ball_x = 250.
        self.ball_y = 100.
        self.ball_x_rotate = 1
        self.ball_y_rotate = 1
        self.ballsize_x = 0.
        self.ballsize_y = 0.
##        self.timer = QTimer()
##        self.timer.timeout.connect(self.timeout)
##        self.timer.setInterval(20)
##        self.timer.start()
        self.evil = QPushButton()
        self.evil.setStyleSheet('''
            background-color: red;
            border-style: outset;
            border-width: 2px;
            border-radius: 10px;
            border-color: beige;
            font: bold 14px;
            min-width: 10em;
            padding: 6px;
        ''')
        

    def init_ui(self):
        self.write_object = WriteObject()
        self.write_thread = QThread(self)
        self.write_thread.start()
        self.write_object.moveToThread(self.write_thread)


   def keyPressEvent(self, event):
       astr = event.nativeScanCode()
       self.keylist.add(astr)

   def keyReleaseEvent(self, event):
       self.keylist.remove(event.nativeScanCode())

   def timeout(self):
       self.update()

   def game(self):
       if 38 in self.keylist and self.Racquet_1_y > 0:
           self.Racquet_1_y = self.Racquet_1_y - 2
       if 52 in self.keylist and self.Racquet_1_y < 160:
           self.Racquet_1_y = self.Racquet_1_y + 2
       if 45 in self.keylist and self.Racquet_2_y > 0:
           self.Racquet_2_y = self.Racquet_2_y - 2
       if 58 in self.keylist and self.Racquet_2_y < 160:
           self.Racquet_2_y = self.Racquet_2_y + 2
       if self.ball_x >= 500:
           self.ball_x_rotate = 0
       if self.ball_x <= 0:
           self.ball_x_rotate = 1
       if self.ball_y >= 200:
           self.ball_y_rotate = 0
       if self.ball_y <= 0:
           self.ball_y_rotate = 1
       if self.ball_x_rotate == 1:
           self.ball_x = self.ball_x + self.x * self.t
       else: self.ball_x = self.ball_x - self.x * self.t
       if self.ball_y_rotate == 1:
           self.ball_y = self.ball_y + self.y * self.t
       else: self.ball_y = self.ball_y - self.y * self.t
       if self.lastx != self.ball_x_rotate or self.lasty != self.ball_y_rotate:
           self.lastx = self.ball_x_rotate
           self.lasty = self.ball_y_rotate
           self.write_object.play_signal.emit(getpath('ball_sounds/hit_0.ogg'))

   # def resizeEvent(self, event):
   #     self.x_arena = self.frameGeometry().width()
   #     self.y_arena = self.frameGeometry().height()
   #     self.gameproc_x = self.x_arena / 500
   #     self.gameproc_y = self.y_arena / 200
   #     self.ballsize_x = self.gameproc_x * 25
   #     self.ballsize_y = self.gameproc_y * 25
   #     self.Racquetssize_x = self.gameproc_x * 9
   #     self.Racquetssize_y = self.gameproc_y * 40
       renderer_s1 = QSvgRenderer(getpath('ball/ball_1.svg'))
       self.ball_image = QImage(int(self.ballsize_x), int(self.ballsize_y), QImage.Format_ARGB32)
       self.ball_image.fill(0x00000000)
       painter = QPainter(self.ball_image)
       renderer_s1.render(painter)
       renderer_s2 = QSvgRenderer(getpath('table/table_0.svg'))
       self.table_image = QImage(int(self.x_arena), int(self.y_arena), QImage.Format_ARGB32)
       self.table_image.fill(0x00000000)
       painter = QPainter(self.table_image)
       renderer_s2.render(painter)
       renderer_s3 = QSvgRenderer(getpath('racquet_blue/racquet_blue_0.svg'))
       self.Racquet_1_image = QImage(int(self.Racquetssize_x), int(self.Racquetssize_y), QImage.Format_ARGB32)
       self.Racquet_1_image.fill(0x00000000)
       painter = QPainter(self.Racquet_1_image)
       renderer_s3.render(painter)
       renderer_s4 = QSvgRenderer(getpath('racquet_red/racquet_red_0.svg'))
       self.Racquet_2_image = QImage(int(self.Racquetssize_x), int(self.Racquetssize_y), QImage.Format_ARGB32)
       self.Racquet_2_image.fill(0x00000000)
       painter = QPainter(self.Racquet_2_image)
       renderer_s4.render(painter)

   def drawall(self, painter):
       painter.drawImage(0, 0, self.table_image)
       painter.drawImage(int(self.ball_x * self.gameproc_x - self.ballsize_x), int(self.ball_y * self.gameproc_y - self.ballsize_y), self.ball_image)
       painter.drawImage(int(3 * self.gameproc_x), int(self.Racquet_1_y * self.gameproc_y), self.Racquet_1_image)
       painter.drawImage(int(489 * self.gameproc_x), int(self.Racquet_2_y * self.gameproc_y), self.Racquet_2_image)

   def paintEvent(self, event):
       painter = QPainter(self)
       self.game()
       self.drawall(painter)
       painter.end()
