from PyQt5.QtGui import QPalette, QTextCharFormat, QColor, QPen
from PyQt5.QtCore import Qt, QDate, QTimer
from PyQt5.QtWidgets import QApplication, QCalendarWidget, QGroupBox, QVBoxLayout, QWidget, QPushButton, QProgressBar, QSizePolicy, QHeaderView, QTableWidget, QTableWidgetItem
from pathlib import Path


def getwidget(penguin, iconintab, textintab, iconsizexy):
    widget = QWidget()
    layout = QVBoxLayout(widget)
    calendar = MyCalendar(penguin, iconintab, textintab, iconsizexy)
    table = MyTimeTable(penguin, iconintab, textintab, iconsizexy)
    layout.addWidget(calendar)
    layout.addWidget(table)
    return widget


class MyTimeTable(QTableWidget):
    def __init__(self, penguin, iconintab, textintab, iconsizexy):
        super().__init__()
        self.setColumnCount(2)
        self.setRowCount(1)
        self.setHorizontalHeaderLabels(["Header 1", "Header 2"])
        # self.horizontalHeaderItem(1).setToolTip("Column 2 ")
        # self.horizontalHeaderItem(2).setToolTip("Column 3 ")
        self.horizontalHeaderItem(0).setTextAlignment(Qt.AlignHCenter)
        self.horizontalHeaderItem(1).setTextAlignment(Qt.AlignHCenter)
        # self.horizontalHeaderItem(2).setTextAlignment(Qt.AlignRight)
        self.setItem(0, 0, QTableWidgetItem("Text in column 1"))
        self.setItem(0, 1, QTableWidgetItem("Text in column 2"))
        # self.setItem(0, 2, QTableWidgetItem("Text in column 3"))
        # self.resizeColumnsToContents()
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)


    #     self.counter = 0
    #     self.timer = QTimer()
    #     self.pbar = QProgressBar(self)
    #     self.pbar.setGeometry(30, 40, 200, 25)
    #     self.pbar.setFixedWidth(400)
    #     self.btn = QPushButton('Start', self)
    #     self.btn.clicked.connect(self.doAction)
    #     self.timer.timeout.connect(self.timeout)
    #     self.setGeometry(300, 300, 280, 170)
    #
    # def doAction(self):
    #     self.timer.setInterval(20)
    #     self.timer.start()
    #     self.counter = 0
    #
    # def timeout(self):
    #     self.pbar.setValue(self.counter)
    #     self.counter = self.counter + 0.1
    #     if self.counter == 101:
    #         self.timer.stop()

class MyCalendar(QCalendarWidget):
    def __init__(self, penguin, iconintab, textintab, iconsizexy):
        super().__init__()
        self.pathtoeventslst = Path('Events calendar.lst')
        self.begin_date = None
        self.end_date = None
        self.days = list()
        self.dates = list()
        self.events = {
            QDate(2021, 5, 4),
            QDate(2021, 5, 10),
            QDate(2021, 5, 11),
            QDate(2021, 5, 25),
            QDate(2021, 5, 31)
         }
        self.setNavigationBarVisible(False)
        self.setGridVisible(True)
        self.highlight_format = QTextCharFormat()
        self.highlight_format.setBackground(self.palette().brush(QPalette.Highlight))
        self.highlight_format.setForeground(self.palette().color(QPalette.HighlightedText))
        self.clicked.connect(self.date_is_clicked)
        print(super().dateTextFormat())

        for d in (Qt.Saturday, Qt.Sunday,):
            fmt = self.weekdayTextFormat(d)
            fmt.setForeground(Qt.black)
            self.setWeekdayTextFormat(d, fmt)

    def format_range(self, format):
        if self.begin_date and self.end_date:
            d0 = min(self.begin_date, self.end_date)
            d1 = max(self.begin_date, self.end_date)
            self.days.clear()
            while d0 <= d1:
                self.days.append(d0)
                self.setDateTextFormat(d0, format)
                d0 = d0.addDays(1)

    def date_is_clicked(self, date):
        self.format_range(QTextCharFormat())
        if QApplication.instance().keyboardModifiers() & Qt.ShiftModifier and self.begin_date:
            self.end_date = date
            self.format_range(self.highlight_format)
        else:
            self.days.clear()
            self.begin_date = date
            self.days.append(self.begin_date)
            self.end_date = None
        self.datelist(self.days)

    def datelist(self, dates):
        print('--------------------------------------------------')
        print(dates)
        self.dates = dates

    def paintCell(self, painter, rect, date):
        QCalendarWidget.paintCell(self, painter, rect, date)
        if date in self.events:
            painter.setPen(QPen(QColor(0, 200, 200), 3, Qt.SolidLine, Qt.SquareCap))
            painter.drawLine(rect.topRight(), rect.topLeft())
            painter.drawLine(rect.topRight(), rect.bottomRight())
            painter.drawLine(rect.bottomLeft(), rect.bottomRight())
            painter.drawLine(rect.topLeft(), rect.bottomLeft())
