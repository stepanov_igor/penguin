import yfinance as yf
import pyqtgraph as pg


def getwidget(self):
    data = yf.download('AMD', '2021-01-01', '2021-05-01')
    intel = yf.download('INTC', '2021-01-01', '2021-05-01')
    tsmc= yf.download('TSM', '2021-01-01', '2021-05-01')
    print(data)
    print(intel)
    plot = pg.PlotWidget()
    #plot.setConfigOptions(antialias=True)

    # pg.setConfigOption('background', 'w')  # background: white
    # pg.setConfigOption('foreground', 'k')  # font: black

    # plot.plot(data['Adj Close'], pen=pg.mkPen('r', width=3))

    plot.setBackground('w')
    # plot.setForeground('k')

    # plot.plot(intel['Adj Close'], pen=pg.mkPen('b', width=3))
    # plot.plot(tsmc['Adj Close'], pen=pg.mkPen('g', width=3))

    # plot.hsvColor(0, 0, 0)
    # plot.getAxis('left').setTextPen(Qt.black)

    plot.show
    return plot