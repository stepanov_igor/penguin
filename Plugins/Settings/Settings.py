#!/usr/bin/python3
import sys
import os
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QPushButton, QScrollArea
from PyQt5.QtGui import QIcon, QStandardItemModel, QStandardItem
from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import QSize, Qt, QTimer


def getwidget(penguin, iconintab, textintab, iconsizexy):
    widget = QWidget()
    sets = Sets(iconintab, textintab, iconsizexy)
    layout = QVBoxLayout(widget)
    layout.addWidget(sets)
    sets.btn.clicked.connect(penguin.reload)
    return widget


def getpath(path):
    root, exe = os.path.split(__file__)
    return root + '/' + path


class Sets(QScrollArea):
    def __init__(self, iconintab, textintab, iconsizexy):
        super().__init__()
        widget = QWidget()
        self.btn = QPushButton()
        lay = QVBoxLayout(widget)
        self.design(self.btn, 'Apply', 'Apply', iconintab, textintab, iconsizexy)
        lay.addWidget(self.btn)
        self.setWidget(widget)
        self.setWidgetResizable(True)

    def design(self, widgetname, text, iconname, iconintab, textintab, iconsizexy):
        widgetname.setIconSize(QSize(iconsizexy, iconsizexy))
        if textintab == str('Yes'):
            widgetname.setText(text)
        if textintab == str('None'):
            widgetname.setText('')
        if iconintab == str('Light'):
            widgetname.setIcon(QIcon(getpath(iconname + '_light.svg')))
        if iconintab == str('Dark'):
            widgetname.setIcon(QIcon(getpath(iconname + '_dark.svg')))

    #     self.pathtoplugsets = Path('Tabs.cfg')
    #     self.pathtotabsets = Path('Sets.cfg')
    #     self.tablist = list()
    #     self.plugins = list()
    #     self.newtabs = list()
    #     self.prenewtabs = list()
    #     self.tabset = list()
    #     self.iconmode = 0
    #     self.textmode = 0
    #     self.thememode = 0
    #     self.iconname = 0
    #
    #     # Plugins listing
    #     files = os.listdir('./Plugins/')
    #     for file in files:
    #         print(file)
    #
    #     def readtabcfg(self):
    #         print("Read tab Cfg is run")
    #         self.prenewtabs = self.pathtoplugsets.read_text().splitlines()
    #         for self.pretab in self.prenewtabs:
    #             if self.pretab == "":
    #                 print("Null line detected")
    #             if self.pretab != "":
    #                 self.newtabs.append(self.pretab)
    #         print(self.newtabs)
    #
    #
    #     self.lv = QtWidgets.QListView()
    #     model = QStandardItemModel()
    #     for tab in files:
    #         item = QStandardItem(tab)
    #         if tab in self.newtabs:
    #             item.setCheckState(QtCore.Qt.Checked)
    #             print("Cheked")
    #         item.setCheckable(True)
    #         model.appendRow(item)
    #         print(tab)
    #     self.lv.setModel(model)
    #     model.itemChanged.connect(self.on_item_changed)
    #     self.applybutton = QPushButton()
    #     self.applybutton.setIconSize(QSize(32, 32))
    #     applyicon = QIcon('Apply.svg')
    #     self.applybutton.setIcon(applyicon)
    #     self.applybutton.setText("Apply")
    #     self.applybutton.setCheckable(False)
    #     self.applybutton.setEnabled(False)
    #     self.applybutton.toggle()
    #     self.applybutton.clicked.connect(self.on_button_pressed)
    #     return self.applybutton, self.lv
    #
    #
    #
    # def readsetcfg(self, param):
    #     print("Read set Cfg is run")
    #     with open(self.pathtotabsets, "r") as infile:
    #         for line in infile:
    #             if line.startswith(param):
    #                 return line.split(" = ")[1]
    #
    #     print("Sets is run")
    #
    #
    #
    #
    #
    #
    #
    #
    # def on_item_changed(self, item):
    #     print("Item is run")
    #     self.applybutton.setEnabled(True)
    #
    # def on_button_pressed(self):
    #     print("Button is run")
    #     model = self.lv.model()
    #     self.plugins.clear()
    #     for index in range(model.rowCount()):
    #         item = model.item(index)
    #         if item.checkState() == QtCore.Qt.Checked:
    #             print(item.text())
    #             self.plugins.append(item.text())
    #     file = self.plugins
    #     print(file)
    #     open('Tabs.cfg', 'w').close()
    #     self.path.write_text('\n'.join(file))
    #
    #     self.applybutton.setEnabled(False)
