import xml.etree.ElementTree as ET
from PyQt5 import QtSvg

def getwidget(self):
    svgWidget = QtSvg.QSvgWidget()
    tree = ET.parse('Penguin.svg')
    elem = tree.findall('''.//*[@id='eye_1']''')[0]
    print(elem)
    elem.set('style', 'fill:#FF0000;stroke-width:0.4725;fill-opacity:1')
    root = tree.getroot()
    xmlstr = ET.tostring(root, encoding='utf8', method='xml')
    svgWidget.load(xmlstr)
    return svgWidget
