#!/usr/bin/python3
import sys
import os
from PyQt5.QtWidgets import QMainWindow, QApplication, QWidget, QTabWidget, QVBoxLayout
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QSize
from pathlib import Path
import importlib


def getpath(path):
    root, exe = os.path.split(__file__)
    return root + '/' + path


class MyTabWidget(QWidget):
    def __init__(self, parent):
        super(QWidget, self).__init__(parent)
        self.layout = QVBoxLayout(self)
        self.configpath = Path(getpath('Penguin.cfg'))
        self.lstpath = Path(getpath('Penguin.list'))
        self.tablist = list()
        self.newtabs = list()
        self.lstoftabs = list()
        self.unlstoftabs = list()
        self.iconmode = 0
        self.iconsizexy = 0
        self.textmode = 0
        self.iconname = 0
        self.movable = 0
        self.tabs = QTabWidget()
        self.tabs.resize(500, 500)
        self.layout.addWidget(self.tabs)
        self.getall()

    def readtablst(self):
        self.lstoftabs.clear()
        self.unlstoftabs.clear()
        files = os.listdir('../Plugins/')
        with open(self.lstpath, 'r') as inlist:
            for line in inlist:
                if line.startswith('tab_name') and line.split(' = ')[2] == 'Enable' and line.split(' = ')[1] in files:
                    self.lstoftabs.append(line.split(' = ')[1])
        self.unlstoftabs = set(files).difference(set(self.lstoftabs))
        os.remove(self.lstpath)
        newfile = open(self.lstpath, 'w')
        for name in self.lstoftabs:
            newfile.write('tab_name = ' + name + ' = Enable = Enable, Disable\n')
        for unname in self.unlstoftabs:
            newfile.write('tab_name = ' + unname + ' = Disable = Enable, Disable\n')
        newfile.close()
        return self.lstoftabs

    def readsetcfg(self, param):
        with open(self.configpath, "r") as infile:
            for line in infile:
                if line.startswith(param):
                    return line.split(" = ")[1]

    def getall(self):
        self.newtabs = self.readtablst()
        print(self.newtabs)
        self.iconmode = self.readsetcfg("set_icon_in_tab")
        print(self.iconmode)
        self.textmode = self.readsetcfg("set_text_in_tab")
        print(self.textmode)
        self.iconsizexy = self.readsetcfg("set_icon_size_x_y")
        print(self.iconsizexy)
        self.movable = self.readsetcfg('set_tabs_movable')
        print(self.movable)
        self.tabsloader(self.newtabs, str(self.iconmode), str(self.textmode), int(self.iconsizexy), str(self.movable))

    def tabsloader(self, listname, iconintab, textintab, iconsizexy, movable):
        for tab in (self.tablist):
            tabindex = tab[0]
            self.tabs.removeTab(tabindex)
            tab[1].deleteLater()
        self.tablist.clear()
        for self.tabname in listname:
            sys.path.insert(0, getpath("../Plugins"))
            self.pluginname = str(self.tabname) + '.' + str(self.tabname)
            module = importlib.import_module(self.pluginname, package=None)
            self.index = list()
            self.tabwidget = module.getwidget(self, iconintab, textintab, iconsizexy)
            print('Movable:', movable)
            if movable == str('Yes'):
                self.tabs.setMovable(True)
            if movable == str('None'):
                self.tabs.setMovable(False)
            print('Text:', textintab)
            if textintab == str('Yes'):
                self.index = self.tabs.addTab(self.tabwidget, self.tabname)
            if textintab == str('None'):
                self.index = self.tabs.addTab(self.tabwidget, str())
            print('Icon:', iconintab)
            if iconintab == str('Light'):
                self.iconname = '../Plugins/' + str(self.tabname) + '/' + str(self.tabname) + str('_light') + '.svg'
            if iconintab == str('Dark'):
                self.iconname = '../Plugins/' + str(self.tabname) + '/' + str(self.tabname) + str('_dark') + '.svg'
            if iconintab == str('Dark') or iconintab == str('Light'):
                self.tabs.setIconSize(QSize(iconsizexy, iconsizexy))
                iconobj = QIcon(getpath(self.iconname))
                self.tabs.setTabIcon(self.index, iconobj)
            self.tablist.append((self.index, self.tabwidget, self.tabname))

    def reload(self):
        self.getall()


class App(QMainWindow):
    def __init__(self):
        super().__init__()
        self.title = 'Penguin'
        self.left = 320
        self.top = 240
        self.width = 600
        self.height = 600
        self.setWindowTitle(self.title)
        window_icon = QIcon(getpath('Penguin.svg'))
        self.setWindowIcon(window_icon)
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.tab_widget = MyTabWidget(self)
        self.setCentralWidget(self.tab_widget)
        self.show()


app = QApplication(sys.argv)
ex = App()
sys.exit(app.exec_())
